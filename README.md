# residualMonitor
PROGRAM: residualMonitor  

DESCRIPTION: A simple real-time residual plotter designed for use in FSI solvers. Place the two files into the fluid folder where your log files are stored and run from command line in a seperate terminal.  

CHANGELOG:

2/11/2019  

Initial Commit  

